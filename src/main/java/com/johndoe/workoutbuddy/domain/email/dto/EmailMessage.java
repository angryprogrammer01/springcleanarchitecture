package com.johndoe.workoutbuddy.domain.email.dto;

public interface EmailMessage {
    String getReceiver();
    String getSubject();
}
