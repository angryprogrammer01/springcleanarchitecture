package com.johndoe.workoutbuddy.domain.user.dto;

public enum GenderDto {
    MALE, FEMALE
}
