package com.johndoe.workoutbuddy.adapter.repository.mongo;

class QueryConstants {
    static final String USERNAME = "username";
    static final String EMAIL = "email";
    static final String TOKEN_ID = "tokenID";
}
